import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import axios from 'axios'
import * as moment from 'moment'
import { API_URL } from '../common/config'

@Component({
  selector: 'chat-details',
  templateUrl: './chatDetails.component.html',
  styleUrls: ['./chatDetails.component.css']
})

export class ChatDetails {
  chatThread = {}
  moment = null
  constructor(private route: ActivatedRoute) {
    this.moment = moment
    this.route.params.subscribe(params => {
      const id = params.id
      if(id) {
        _getChatDetails(id)
          .then((res) => {
            this.chatThread = res
          })
      }
    })
  }
}

const _getChatDetails = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${API_URL}chats/${id}`)
      .then((payload) => {
        return resolve(payload.data)
      })
      .catch((error) => {
        return reject(error)
      })
  })
}
