import { Component } from '@angular/core';
import axios from 'axios'
import * as moment from 'moment'
import { API_URL } from '../common/config'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'chat-room',
  templateUrl: './chatRooms.component.html',
  styleUrls: ['./chatRooms.component.css']
})

export class ChatRooms {
  title = 'app'
  activeChat:any
  moment = null
  id:number

  constructor(private route: ActivatedRoute, private router: Router) {
    this.moment = moment
    this.route.params.subscribe(params => {
      this.id = params.id
    })
    _getActiveChat()
      .then((res) => {
        if(!this.id) {
          this.router.navigate(['/chats', res[0]['id']])
        }
        this.activeChat = res
      })
  }
}

const _getActiveChat = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${API_URL}active-chats`)
      .then((payload) => {
        return resolve(payload.data)
      })
      .catch((error) => {
        return reject(error)
      })
  })
}
