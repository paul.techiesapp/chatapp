import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { ChatRooms } from './chatRooms/chatRooms.component'
import { ChatDetails } from './chatDetails/chatDetails.component'


const appRoutes: Routes = [
  { path: 'chats', component: ChatRooms },
  { path: 'chats/:id', component: ChatRooms },
  { path: '**', component: ChatRooms }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule
  ],
  declarations: [
    AppComponent, ChatRooms, ChatDetails
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
